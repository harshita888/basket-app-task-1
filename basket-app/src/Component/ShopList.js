import React from 'react';
import './ShopList.css';
import {FaPlusSquare} from 'react-icons/fa';

const shoplist = (props) => {
    return (
        <div className="shop-list-block" >
            <ul >

                <li  onClick={()=>{props.click(props.index)}}><FaPlusSquare className="plus1"  />{props.name}</li>
            </ul>
        </div>
    );

}
export default shoplist;