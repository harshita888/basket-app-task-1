import React from 'react';
import './Basket.css';
import {FaMinusSquare} from 'react-icons/fa';

const basket = (props) => {
    return (
        <div className="basket-block">

            <ul >
                <li onClick={()=>{props.click(props.index)}}><FaMinusSquare className="minus" />{props.count+' '+props.name}</li>
            </ul>
        </div>

    );

}
export default basket;