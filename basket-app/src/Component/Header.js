import React from 'react';
import './Header.css';
import {FaShoppingBasket} from 'react-icons/fa';
const header = (props) => {
  return (
    <div className="App">
      
      <header className="App-header">
      <FaShoppingBasket size={150} />
      <h1 className="App-title">Hello, Basket!</h1>
      </header>
      
    </div>
  );
}

export default header;