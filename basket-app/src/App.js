import React, { Component } from 'react';
import Header from './Component/Header';
import Search from './Component/Search';
import ShopList from './Component/ShopList';
import Basket from './Component/Basket';
import Footer from './Component/Footer';
import { FaLeaf, FaPlusSquare, FaMinusSquare, FaShoppingBasket,  FaTrash } from 'react-icons/fa';

import './App.css';
import shoplist from './Component/ShopList';




class App extends Component {
  state = {
    list: [
      { name: "Strawberry", id: 1 },
      { name: "Blueberry", id: 2 },
      { name: "Orange", id: 3 },
      { name: "Banana", id: 4 },
      { name: "Apple", id: 5 },
      { name: "Carrot", id: 6 },
      { name: "Celery", id: 7 },
      { name: "Mushroom", id: 8 },
      { name: "GreenPepper", id: 9 },
      { name: "Eggs", id: 10 },
      { name: "Cheese", id: 11 },
      { name: "Butter", id: 12 },
      { name: "Chicken", id: 13 },
      { name: "Beef", id: 14 },
      { name: "Pork", id: 15 },
      { name: "Fish", id: 16 },
      { name: "Rice", id: 17 },
      { name: "Pasta", id: 18 },
      { name: "Bread", id: 19 }
    ],
    newlist: [
      { name: "Strawberry", id: 1 ,count:1},
      { name: "Blueberry", id: 2 ,count:4},
      { name: "Orange", id: 3 ,count:2},
    ]
  }
  newBasket = (index) => {
    
        let shopList=this.state.list[index];
        let flag=-1;
    console.log(shopList);
   
        for(let item in this.state.newlist)
        {
          if(this.state.newlist[item].id===shopList.id)
            {
              flag=item;
              console.log(flag);
              break;
            } 
        }

        if(flag!==-1)
        {
          let basket=[...this.state.newlist];
          basket[flag].count++;
          this.setState({
            newlist:basket
          });

        }
        else
        {
          shopList.count=1;
          let basket=[...this.state.newlist];
          basket.push(shopList);
          this.setState({
            newlist:basket
          });

        }
      //console.log("yes");
     
  }
  
  
  strikes=(index)=>{
    let shop=this.state.newlist[index];
    let flag=-1;
    console.log(shop);
    
    for(let item in this.state.newlist)
        {
          if(this.state.newlist[item].id===shop.id)
            {
              flag=item;
              console.log(flag);
              break;
            } 
        }
        if(flag>=0)
        {
          let basket=[...this.state.newlist];
          basket[flag].count--;
          if(shop.count>=0){
            this.setState({
              newlist:basket
            });
          }
          else{

            shop.count=0;
            this.setState({
              newlist:basket
            });

          }
          

        }
        
        // else
        // {
        //   shop.count=0;
        //   let basket=[...this.state.newlist];
        //   basket.push(shop);
        //   this.setState({
        //     newlist:basket
        //   });

        // }
    // console.log(flag);
    // shopList={};
    // this.setState({
    //   newlist:[{shopList}]
    // });
  }
  delete=()=>{
    let arr = [{name:"Your basket is empty!", count: " "}];
    
    this.setState({
      newlist:arr
    });
  }
  render() {

    let grocery = (
      <div>
        {
          this.state.list.map((item, index) => {
            return (
              <div key={item.id} className="groceries">
                <ShopList name={item.name} id={item.id} index={index} click={this.newBasket} />
                {/* <button className="plus" id={item.id} onClick={this.newBasket(item.id)}> */}
                
              </div>
            );
          })
        }
      </div>
    );
    const basket = (
      <div className="basket-item">
        {
          this.state.newlist.map((item, index)=>{
            return(
              <div  key={item.id} className="basket-list">
                <Basket name={item.name} id={item.id} index={index} count={item.count} click={this.strikes} />
                
                </div>
            );
          })
        }
      </div>
    )
    return (
      <div className="App">

        <Header />

        <Search />
        <div className="set">
          <div className="groceries-heading"><FaLeaf /> <b>Groceries</b></div>
          {grocery}
        </div>
        <div className="basket">
          <div className="basket-heading"><FaShoppingBasket /> <b>Basket</b></div><FaTrash className="trash" onClick={this.delete}/>
          {basket}
          
        </div>
      
        <Footer />
      
      </div>

    );
  }
}

export default App;
